package migrations_interface

import (
	"gin/core"
)

type MigrationHandleFunc func(c *core.Core, args ...string) error

type MigrationInterface interface {
	Up() MigrationHandleFunc
	Down() MigrationHandleFunc
}
