package migrations

import (
	"gin/domain/tenancy/models"
	"gin/migrations/databases"
	"gin/migrations/interface"
	"gin/migrations/script"
)

func Dispatches() map[string]migrations_interface.MigrationInterface {
	return map[string]migrations_interface.MigrationInterface{
		"demo":             migrations_script.Demo{},
		"databases_demo":   migrations_databases.Demo{},
		"init_tenant_corp": model_tenancy.TenantCorp{},
	}
}

func Dispatcher(action string) migrations_interface.MigrationInterface {
	m, has := Dispatches()[action]
	if !has {
		return nil
	}
	return m
}
