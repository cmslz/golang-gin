package migrations_script

import (
	"gin/core"
	migrations_interface "gin/migrations/interface"
)

type Demo struct {
}

func (d Demo) Up() migrations_interface.MigrationHandleFunc {
	return func(c *core.Core, args ...string) error {
		c.Logger.Info("demo up")
		return nil
	}
}

func (d Demo) Down() migrations_interface.MigrationHandleFunc {
	return func(c *core.Core, args ...string) error {
		c.Logger.Info("demo down")
		return nil
	}
}
