package migrations_databases

import (
	"gin/core"
	"gin/domain/tenancy/services"
	"gin/migrations/interface"
	"time"
)

type Demo struct {
}

func (d Demo) Up() migrations_interface.MigrationHandleFunc {
	return func(c *core.Core, args ...string) error {
		service_tenancy.AllRunForMultiple(c, func(c *core.Core) {
			db := c.TenancyDb
			type Like struct {
				ID        int    `gorm:"primary_key"`
				Ip        string `gorm:"type:varchar(20);not null;index:ip_idx"`
				Ua        string `gorm:"type:varchar(256);not null;"`
				Title     string `gorm:"type:varchar(128);not null;index:title_idx"`
				Hash      uint64 `gorm:"unique_index:hash_idx;"`
				CreatedAt time.Time
			}
			if !db.HasTable(&Like{}) {
				if err := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='喜欢'").CreateTable(&Like{}).Error; err != nil {
					panic(err)
				}
			}
		})
		return nil
	}
}

func (d Demo) Down() migrations_interface.MigrationHandleFunc {
	return func(c *core.Core, args ...string) error {
		service_tenancy.AllRunForMultiple(c, func(c *core.Core) {
			db := c.TenancyDb
			type Like struct {
				ID        int    `gorm:"primary_key"`
				Ip        string `gorm:"type:varchar(20);not null;index:ip_idx"`
				Ua        string `gorm:"type:varchar(256);not null;"`
				Title     string `gorm:"type:varchar(128);not null;index:title_idx"`
				Hash      uint64 `gorm:"unique_index:hash_idx;"`
				CreatedAt time.Time
			}
			db.DropTableIfExists(&Like{})
		})
		return nil
	}
}
