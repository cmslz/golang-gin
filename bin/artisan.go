package main

import (
	"fmt"
	"gin/core"
	"gin/core/amqp/queue"
	"gin/migrations"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf(fmt.Sprintf("参数异常"))
	}

	// 初始化核心
	c := core.New(nil)
	c.Init()
	action := os.Args[1]
	//fmt.Println(fmt.Sprintf("action:%s args:%s", action, os.Args))
	switch action {
	case "migrate/up":
		if err := migrations.Dispatcher(os.Args[2]).Up()(c, os.Args[3:]...); err != nil {
			c.Logger.Error(err)
			os.Exit(1)
		}
	case "migrate/down":
		if err := migrations.Dispatcher(os.Args[2]).Down()(c, os.Args[3:]...); err != nil {
			c.Logger.Error(err)
			os.Exit(1)
		}
	case "work:queue":
		if err := queue.Dispatcher(os.Args[2]).Run()(c, os.Args[3:]...); err != nil {
			c.Logger.Error(err)
			os.Exit(1)
		}
	}
}
