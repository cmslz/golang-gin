package router_corp

import (
	"gin/core"
	"gin/domain/corp/ports/merchant"
	"gin/domain/tenancy/middleware"
)

func Router(c *core.Core) {
	c.Engine.Use(middleware_tenancy.MiddleWare(c))
	corp := c.Engine.Group("/corp")
	{
		merchant_corp.Router(c, corp)
	}
}
