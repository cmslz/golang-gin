package CorpController

import (
	"fmt"
	"gin/core"
	"gin/core/amqp"
	"gin/core/amqp/queue/bin"
	"gin/core/help"
	"gin/domain/corp/models"
	"gin/domain/corp/repositories"
	"github.com/gin-gonic/gin"
	"time"
)

func Demo(c *core.Core) gin.HandlerFunc {
	return func(i *gin.Context) {
		workCorp, err := repositorie_corp.FindFirstCorp(c)
		if err != nil {
			help.Error(i, 500, fmt.Sprintf("%s", err))
			return
		}
		type Result struct {
			model_corp.WorkCorp
		}
		c.Cache.Set("Hello", "中央域", time.Second+60)
		c.TenancyCache.Set("HelloW", "租户", time.Second+60)
		var resultWorkCorp = Result{workCorp}
		Demo := amqp.PublishConfig{
			QueueName: "demo",
			Content: amqp.Content{
				TenancyId: c.TenancyId,
				Content:   bin.Demo{Id: 1, Name: "adasdadad"},
			},
		}
		err = amqp.PublishSimple(c.Logger, c.RabbitMQ, Demo)
		if err != nil {
			c.Logger.Fatalf(fmt.Sprintf("%s", err))
		}
		help.Success(i, resultWorkCorp)
	}
}
