package merchant_corp

import (
	"gin/core"
	"gin/domain/corp/ports/merchant/controllers"
	"github.com/gin-gonic/gin"
)

func Router(c *core.Core, r *gin.RouterGroup) {
	merchant := r.Group("/merchant")
	{
		merchant.GET("/demo", CorpController.Demo(c))
	}
}
