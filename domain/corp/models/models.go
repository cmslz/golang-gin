package model_corp

func (*WorkCorp) TableName() string {
	return "work_corp"
}

type WorkCorp struct {
	Id             uint   `gorm:"PRIMARY_KEY;auto_increment;NOT NULL;column:id;type:int(10);unsigned;" json:"id" xml:"id" yaml:"id"`
	Name           string `gorm:"NOT NULL;column:name;type:varchar(100);" json:"name" xml:"name" yaml:"name"`
	WorkCorpId     string `gorm:"NOT NULL;column:work_corp_id;type:varchar(20);" json:"work_corp_id" xml:"work_corp_id" yaml:"work_corp_id"`
	DeptType       uint   `gorm:"NOT NULL;column:dept_type;type:tinyint(1);unsigned;" json:"dept_type" xml:"dept_type" yaml:"dept_type"`
	SyncStatus     uint   `gorm:"NOT NULL;column:sync_status;type:tinyint(1);unsigned;" json:"sync_status" xml:"sync_status" yaml:"sync_status"`
	RootWorkDeptId uint   `gorm:"NULL;column:root_work_dept_id;type:int(10);unsigned;" json:"root_work_dept_id" xml:"root_work_dept_id" yaml:"root_work_dept_id"`
	IsBind         uint   `gorm:"NOT NULL;column:is_bind;type:tinyint(1);unsigned;" json:"is_bind" xml:"is_bind" yaml:"is_bind"`
	CreatedAt      string `gorm:"NULL;column:created_at;type:timestamp;" json:"created_at" xml:"created_at"`
	UpdatedAt      string `gorm:"NULL;column:updated_at;type:timestamp;" json:"updated_at" xml:"updated_at"`
	DeletedAt      string `gorm:"NULL;column:deleted_at;type:timestamp;" json:"deleted_at" xml:"deleted_at"`
}
