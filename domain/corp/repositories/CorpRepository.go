package repositorie_corp

import (
	"errors"
	"gin/core"
	"gin/domain/corp/models"
)

// FindFirstCorp // 获取首个企业信息
func FindFirstCorp(c *core.Core) (model_corp.WorkCorp, error) {
	workCorp := model_corp.WorkCorp{}
	c.TenancyDb.Table(workCorp.TableName()).Where("deleted_at IS NULL").Find(&workCorp)
	if workCorp.Id == 0 {
		return workCorp, errors.New("FindFirstCorp 不存在")
	}
	return workCorp, nil
}
