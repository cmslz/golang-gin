package touter_tenancy

import (
	"gin/core"
	"gin/domain/tenancy/ports/platform"
)

func Router(c *core.Core) {
	tenancy := c.Engine.Group("/tenancy")
	{
		platform_tenancy.Router(c, tenancy)
	}
}
