package model_tenancy

import "database/sql"

// DomainTableName // 表名
func (*Domain) DomainTableName() string {
	return "tenant_domains"
}

// Domain // 表实体
type Domain struct {
	Id        uint           `gorm:"NOT NULL;column:id;type:int unsigned AUTO_INCREMENT;comment:'域名id';PRIMARY_KEY;" json:"id" xml:"id"`
	Domain    string         `gorm:"NOT NULL;column:domain;type:varchar(128);comment:'域名';" json:"domain" xml:"domain"`
	TenantId  string         `gorm:"NOT NULL;column:tenant_id;type:varchar(32);comment:'租户id';" json:"tenant_id" xml:"tenant_id"`
	CreatedAt string         `gorm:"NOT NULL;column:created_at;type:timestamp;comment:'创建时间';" json:"created_at" xml:"created_at"`
	UpdatedAt sql.NullString `gorm:"NULL;column:updated_at;type:timestamp;comment:'修改时间';" json:"updated_at" xml:"updated_at"`
	DeletedAt sql.NullString `gorm:"NULL;column:deleted_at;type:timestamp;comment:'删除时间';" json:"deleted_at" xml:"deleted_at"`
}
