package model_tenancy

import "database/sql"

// TableName // 表名
func (*Tenancys) TableName() string {
	return "tenants"
}

// Tenancys // 表实体
type Tenancys struct {
	Id        string         `gorm:"PRIMARY_KEY;NOT NULL;column:id;type:varchar(32);comment:'租户id';" json:"id" xml:"id"`
	Data      string         `gorm:"null;column:data;type:json;comment:'配置项';" json:"data" xml:"data" yaml:"data"`
	CreatedAt string         `gorm:"NOT NULL;column:created_at;type:timestamp;comment:'创建时间';" json:"created_at" xml:"created_at"`
	UpdatedAt sql.NullString `gorm:"NULL;column:updated_at;type:timestamp;comment:'修改时间';" json:"updated_at" xml:"updated_at"`
	DeletedAt sql.NullString `gorm:"NULL;column:deleted_at;type:timestamp;comment:'删除时间';" json:"deleted_at" xml:"deleted_at"`
}

type TenancysData struct {
	TenancyDbName     string `json:"tenancy_db_name" yaml:"tenancy_db_name" xml:"tenancy_db_name"`
	OpenPlatformAppid string `json:"open_platform_appid" yaml:"open_platform_appid" xml:"open_platform_appid"`
}
