package model_tenancy

import (
	"database/sql"
	"gin/core"
	"gin/migrations/interface"
)

const (
	TENANT_CORP_STATUS_AWAIT_HANDLE = 0 // 开户中
	TENANT_CORP_STATUS_NORMAL       = 1 // 正常
	TENANT_CORP_STATUS_DISABLE      = 2 // 停用
)

func (t *TenantCorp) TenantCorpTableName() string {
	return "tenant_corp"
}

type TenantCorp struct {
	Id               uint           `gorm:"NOT NULL;column:id;type:int unsigned AUTO_INCREMENT;comment:'企业id';PRIMARY_KEY;" json:"id" xml:"id"`
	TenantId         string         `gorm:"NOT NULL;column:tenant_id;type:varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'租户id';" json:"tenant_id" xml:"tenant_id"`
	Logo             string         `gorm:"NULL;column:logo;type:varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'企业LOGO';" json:"logo" xml:"logo"`
	Name             string         `gorm:"NOT NULL;column:name;type:varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'企业名称';" json:"name" xml:"name"`
	ShortName        string         `gorm:"NOT NULL;column:short_name;type:varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'企业简称';" json:"short_name" xml:"short_name"`
	QrCode           string         `gorm:"NULL;column:qr_code;type:varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'企业二维码';" json:"qr_code" xml:"qr_code"`
	Contact          string         `gorm:"NULL;column:contact;type:varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'联系人';" json:"contact" xml:"contact"`
	ContactTelephone string         `gorm:"NULL;column:contact_telephone;type:varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'联系号码';" json:"contact_telephone" xml:"contact_telephone"`
	District         uint           `gorm:"NULL;column:district;type:smallint unsigned;comment:'地区编码';" json:"district" xml:"district"`
	Longitude        string         `gorm:"NULL;column:longitude;type:decimal(10,6);comment:'经度';" json:"longitude" xml:"longitude"`
	Latitude         string         `gorm:"NULL;column:latitude;type:decimal(10,6);comment:'维度';" json:"latitude" xml:"latitude"`
	Address          string         `gorm:"NULL;column:address;type:varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;comment:'详细地址';" json:"address" xml:"address"`
	Config           string         `gorm:"NULL;column:config;type:json;comment:'配置项';" json:"config" xml:"config"`
	Status           uint           `gorm:"column:status;type:smallint unsigned;NOT NULL;DEFAULT 0;comment:'状态';" json:"status" xml:"status"`
	CreatedAt        string         `gorm:"NOT NULL;column:created_at;type:timestamp;comment:'创建时间';" json:"created_at" xml:"created_at"`
	UpdatedAt        sql.NullString `gorm:"NULL;column:updated_at;type:timestamp;comment:'修改时间';" json:"updated_at" xml:"updated_at"`
	DeletedAt        sql.NullString `gorm:"NULL;column:deleted_at;type:timestamp;comment:'删除时间';" json:"deleted_at" xml:"deleted_at"`
}

type TenantCorpConfig struct {
	StartAt         string `yaml:"start_at" xml:"start_at" json:"start_at"`
	ExpiredAt       string `yaml:"expired_at" xml:"expired_at" json:"expired_at"`
	RecordId        string `yaml:"record_id" xml:"record_id" json:"record_id"`
	FrontRecordId   string `yaml:"front_record_id" xml:"front_record_id" json:"front_record_id"`
	DomainPrefix    string `yaml:"domain_prefix" xml:"domain_prefix" json:"domain_prefix"`
	WebDomainPrefix string `yaml:"web_domain_prefix" xml:"web_domain_prefix" json:"web_domain_prefix"`
}

// OpenCorpQueue // 开户队列
type OpenCorpQueue struct {
	Id uint
}

func (t TenantCorp) Up() migrations_interface.MigrationHandleFunc {
	return func(c *core.Core, args ...string) error {
		c.Db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='租户-企业表'").AutoMigrate(t)
		return nil
	}
}

func (t TenantCorp) Down() migrations_interface.MigrationHandleFunc {
	return func(c *core.Core, args ...string) error {
		c.Db.DropTableIfExists(t)
		return nil
	}
}
