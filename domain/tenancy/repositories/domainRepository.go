package repositorie_tenancy

import (
	"errors"
	"fmt"
	"gin/core"
	"gin/domain/tenancy/models"
)

// TenancyIdByDomain // 域名获取租户id
func TenancyIdByDomain(c *core.Core, domain string) (string, error) {
	domainModel := DomainInfoByDomain(c, domain)
	if domainModel.Id == 0 {
		return domainModel.TenantId, errors.New(domain + " 不存在")
	}
	return domainModel.TenantId, nil
}

func DomainInfoByDomain(c *core.Core, domain string) *model_tenancy.Domain {
	domainModel := model_tenancy.Domain{}
	c.Db.Table(domainModel.DomainTableName()).Where("domain = ? AND deleted_at IS NULL", domain).Limit(1).Find(&domainModel)
	return &domainModel
}

// AddDomain // 添加租户域名
func AddDomain(c *core.Core, domain string, tenantId string) (*model_tenancy.Domain, error) {
	if dm := DomainInfoByDomain(c, domain); dm.Id > 0 {
		return nil, errors.New(fmt.Sprintf("租户域名：%s 已存在", domain))
	}
	domainModel := &model_tenancy.Domain{
		Domain:   domain,
		TenantId: tenantId,
	}
	c.Db.Table(domainModel.DomainTableName()).Create(&domainModel)
	if domainModel.Id == 0 {
		return nil, errors.New("租户域名注册失败")
	}
	return domainModel, nil
}
