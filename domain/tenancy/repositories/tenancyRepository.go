package repositorie_tenancy

import (
	"encoding/json"
	"errors"
	"fmt"
	"gin/core"
	"gin/core/databases"
	"gin/domain/tenancy/models"
	"github.com/jinzhu/gorm"
)

// TenancyByTenancyId // TenancyId 获取详情
func TenancyByTenancyId(c *core.Core, tenancyId string) (model_tenancy.Tenancys, error) {
	Tenancys := model_tenancy.Tenancys{}
	c.Db.Table(Tenancys.TableName()).Where("id = ? AND deleted_at IS NULL", tenancyId).Find(&Tenancys)
	if Tenancys.Id == "" {
		return Tenancys, errors.New("Tenancy:" + tenancyId + "不存在")
	}
	return Tenancys, nil
}

// TenantDataToObject // tenantData转对象
func TenantDataToObject(data string) (model_tenancy.TenancysData, error) {
	var Data model_tenancy.TenancysData
	if data == "" {
		return Data, nil
	}
	err := json.Unmarshal([]byte(data), &Data)
	if err != nil {
		return Data, errors.New(fmt.Sprintf("TenantDataToObject err:%s", err))
	}
	return Data, nil
}

// AllTenantIds // 获取所有租户ids
func AllTenantIds(c *core.Core) []string {
	Tenancys := model_tenancy.Tenancys{}
	var list []model_tenancy.Tenancys
	c.Db.Table(Tenancys.TableName()).Where("deleted_at IS NULL").Select("id").Find(&list)
	var tenantIds []string
	for _, v := range list {
		tenantIds = append(tenantIds, v.Id)
	}
	return tenantIds
}

// ExistByTenantId // TenantId是否存在
func ExistByTenantId(c *core.Core, tenantId string) bool {
	Tenancys := model_tenancy.Tenancys{}
	c.Db.Table(Tenancys.TableName()).Where("id = ?", tenantId).Select("id").First(&Tenancys)
	if Tenancys.Id == "" {
		return false
	}
	return true
}

// CreateTenancy // 创建租户
func CreateTenancy(c *core.Core, tenancys *model_tenancy.Tenancys) (*model_tenancy.Tenancys, error) {
	if ExistByTenantId(c, tenancys.Id) {
		return nil, errors.New("租户Id重复")
	}
	return tenancys, nil
}

// InitDatabase // 初始化租户数据库
func InitDatabase(c *core.Core, tenantId string) error {
	// 链接基础数据库
	baseDb := &gorm.DB{}
	baseDatabase := "tenant_base"
	baseDb = databases.RegisterDb(baseDb, c.Configure.Debug, c.Configure.Database.IdlePool, c.Configure.Database.Pool, c.Logger, fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=%s", c.Configure.Database.Username, c.Configure.Database.Password, c.Configure.Database.Host, c.Configure.Database.Port, baseDatabase, c.Configure.Database.Charset))

	// 创建新租户数据库以及链接新租户数据库
	var newDatabase = "tenant_" + tenantId
	c.Db.Exec(fmt.Sprintf("CREATE DATABASE If Not Exists `%s` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci", newDatabase))
	newDb := &gorm.DB{}
	newDb = databases.RegisterDb(baseDb, c.Configure.Debug, c.Configure.Database.IdlePool, c.Configure.Database.Pool, c.Logger, fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=%s", c.Configure.Database.Username, c.Configure.Database.Password, c.Configure.Database.Host, c.Configure.Database.Port, newDatabase, c.Configure.Database.Charset))

	// 查出基础表所有表
	var table string
	tableRows, err := baseDb.Raw("show tables;").Rows()
	if err != nil {
		c.Logger.Error(fmt.Sprintf("show tables err%s", err))
		baseDb.Close()
		newDb.Close()
		return err
	}
	// 循环获取所有表
	for tableRows.Next() {
		err = tableRows.Scan(&table)
		if err != nil {
			baseDb.Close()
			newDb.Close()
			return err
		}
		// 新租户表不存在创建表
		if newDb.HasTable(table) != true {
			//同步表结构
			newDb.Exec(fmt.Sprintf("CREATE TABLE `%s` LIKE `%s`.`%s`", table, baseDatabase, table))
		}
		// 同步表数据
		newDb.Exec(fmt.Sprintf("Replace into `%s` select * FROM `%s`.`%s`", table, baseDatabase, table))
	}
	baseDb.Close()
	newDb.Close()
	return nil
}
