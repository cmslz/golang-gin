package repositorie_tenancy

import (
	"encoding/json"
	"errors"
	"gin/core"
	"gin/domain/tenancy/models"
	"github.com/mozillazg/go-pinyin"
	"strconv"
	"strings"
)

// GenerateTenantId // 生成tenantId
func GenerateTenantId(c *core.Core, shortName string) string {
	py := pinyin.NewArgs()
	py.Separator = ""
	tenantId := strings.ToLower(pinyin.Slug(shortName, py))
	var i = 0
	for {
		if !CorpExistByTenantId(c, tenantId) {
			break
		}
		tenantId += strconv.Itoa(i)
		i++
	}
	return tenantId
}

// OpenCorp // 开户
func OpenCorp(c *core.Core, corp *model_tenancy.TenantCorp) (*model_tenancy.TenantCorp, error) {
	corp.TenantId = GenerateTenantId(c, corp.ShortName)
	if corp.Config != "" {
		config := new(model_tenancy.TenantCorpConfig)
		err := json.Unmarshal([]byte(corp.Config), config)
		if err != nil {
			return nil, errors.New("config结构异常")
		}
	}
	c.Db.Table(corp.TenantCorpTableName()).Create(&corp)
	if corp.Id == 0 {
		return nil, errors.New("开户失败")
	}
	return corp, nil
}

// ExistByMobile // 手机号码是否存在
func ExistByMobile(c *core.Core, mobile string) bool {
	corp := model_tenancy.TenantCorp{}
	c.Db.Table(corp.TenantCorpTableName()).Where("contact_telephone = ?", mobile).Select("id").Limit(1).First(&corp)
	if corp.Id == 0 {
		return false
	}
	return true
}

// CorpExistByTenantId // TenantId是否存在
func CorpExistByTenantId(c *core.Core, tenantId string) bool {
	corp := model_tenancy.TenantCorp{}
	c.Db.Table(corp.TenantCorpTableName()).Where("tenant_id = ?", tenantId).Select("id").Limit(1).First(&corp)
	if corp.Id == 0 {
		return false
	}
	return true
}

// CorpInfoById // id获取企业详情
func CorpInfoById(c *core.Core, id uint) (*model_tenancy.TenantCorp, error) {
	corp := &model_tenancy.TenantCorp{}
	c.Db.Table(corp.TenantCorpTableName()).Where("id = ?", id).Limit(1).First(&corp)
	if corp.Id == 0 {
		return nil, errors.New("企业不存在")
	}
	return corp, nil
}

// CorpUpdateById // id更新企业信息
func CorpUpdateById(c *core.Core, id uint, corp *model_tenancy.TenantCorp) error {
	c.Db.Table(corp.TenantCorpTableName()).Where("id = ?", id).Update(&corp)
	return nil
}
