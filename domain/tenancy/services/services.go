package service_tenancy

import (
	"fmt"
	"gin/core"
	"gin/core/cache"
	"gin/core/databases"
	"gin/core/help"
	"gin/core/logger"
	repositorie_tenancy "gin/domain/tenancy/repositories"
	"log"
)

// RegisterTenancyDb // 注册租户数据库
func RegisterTenancyDb(c *core.Core, TenancyId string) *core.Core {
	c.TenancyDb = databases.RegisterDb(c.TenancyDb, c.Configure.Debug, c.Configure.Database.IdlePool, c.Configure.Database.Pool, c.Logger, BuildDomainMysqlConn(c, TenancyId))
	return c
}

// RegisterTenancyRedis // 注册租户redis
func RegisterTenancyRedis(c *core.Core, TenancyId string) *core.Core {
	c.TenancyRedisClient, c.TenancyCache = cache.RegisterRedis(c.TenancyRedisClient, c.TenancyCache, c.Configure, TenancyId)
	return c
}

func BuildDomainMysqlConn(c *core.Core, TenancyId string) string {
	tenancys, err := repositorie_tenancy.TenancyByTenancyId(c, TenancyId)
	if err != nil {
		help.Error(c.GinContext, 500, "TenancyId 不存在")
		c.GinContext.Next()
		log.Fatalf(fmt.Sprintf("BuildDomainMysqlConn err:%s", err))
	}
	TenancyData, _ := repositorie_tenancy.TenantDataToObject(tenancys.Data)
	return fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=%s", c.Configure.Database.Username, c.Configure.Database.Password, c.Configure.Database.Host, c.Configure.Database.Port, TenancyData.TenancyDbName, c.Configure.Database.Charset)
}

type RunForMultipleCallback func(*core.Core)

// AllRunForMultiple // 所有租户运行
func AllRunForMultiple(c *core.Core, callback RunForMultipleCallback) {
	tenantIds := repositorie_tenancy.AllTenantIds(c)
	RunFormMultiple(c, tenantIds, callback)
}

// Init // 初始化
func Init(c *core.Core, TenancyId string) *core.Core {
	if c.TenancyId != TenancyId {
		c.Logger = logger.RegisterLogger(c.Logger, c.Configure, c.ElasticSearch, TenancyId)
		if TenancyId != "" {
			c = RegisterTenancyDb(c, TenancyId)
			c = RegisterTenancyRedis(c, TenancyId)
		} else {
			c.TenancyDb = nil
			c.TenancyRedisClient = nil
			c.TenancyCache = nil
		}
	}
	return c
}

// RunFormMultiple //
func RunFormMultiple(c *core.Core, tenantIds []string, callback RunForMultipleCallback) {
	nowCore := c
	for _, tenantId := range tenantIds {
		Init(c, tenantId)
		callback(c)
	}
	// 复原原链接
	c = nowCore
}
