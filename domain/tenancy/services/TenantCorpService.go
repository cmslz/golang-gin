package service_tenancy

import (
	"encoding/json"
	"errors"
	"fmt"
	"gin/core"
	"gin/core/aliyun"
	"gin/core/amqp"
	"gin/core/configure"
	"gin/domain/tenancy/models"
	"gin/domain/tenancy/repositories"
	"github.com/jinzhu/gorm"
	"go.uber.org/zap"
)

type OpenTenant struct {
	DB          *gorm.DB
	Logger      *zap.SugaredLogger
	Configure   *configure.Configure
	nowTenantDb *gorm.DB
	TenantId    string
	CorpName    string
	Mobile      string
}

// OpenCorp // 开户
func OpenCorp(c *core.Core, corp *model_tenancy.TenantCorp) (*model_tenancy.TenantCorp, error) {
	if repositorie_tenancy.ExistByMobile(c, corp.ContactTelephone) {
		return nil, errors.New("开户手机号已被使用")
	}
	corpConfig := CorpConfig(corp.Config)
	config, err := json.Marshal(corpConfig)
	if err != nil {
		return nil, err
	}
	corp.Config = string(config)
	corp.Status = model_tenancy.TENANT_CORP_STATUS_AWAIT_HANDLE
	corp, err = repositorie_tenancy.OpenCorp(c, corp)
	if err != nil {
		return nil, err
	}
	type OpenCorp struct {
		Id uint
	}
	OpenCorpQueueConfig := amqp.PublishConfig{
		QueueName: amqp.TenancyOpenCorpQueue,
		Content: amqp.Content{
			TenancyId: c.TenancyId,
			Content:   OpenCorp{Id: corp.Id},
		},
	}
	err = amqp.PublishSimple(c.Logger, c.RabbitMQ, OpenCorpQueueConfig)
	if err != nil {
		c.Logger.Fatalf(fmt.Sprintf("%s", err))
		return nil, err
	}
	return corp, nil
}

// CorpConfig // json转换
func CorpConfig(config string) *model_tenancy.TenantCorpConfig {
	configJson := &model_tenancy.TenantCorpConfig{}
	_ = json.Unmarshal([]byte(config), &configJson)
	return configJson
}

func InitCorp(c *core.Core, id uint) error {
	corp, err := repositorie_tenancy.CorpInfoById(c, id)
	if err != nil {
		return err
	}
	// 初始化租户数据库
	err = repositorie_tenancy.InitDatabase(c, corp.TenantId)
	if err != nil {
		return err
	}
	corpConfig := CorpConfig(corp.Config)

	// 设置前端 后端域名解析
	corpConfig.DomainPrefix = corp.TenantId + "-api"
	corpConfig.WebDomainPrefix = corp.TenantId
	if c.Configure.Env != "production" {
		corpConfig.DomainPrefix = c.Configure.Env + "-" + corpConfig.DomainPrefix
		corpConfig.WebDomainPrefix = c.Configure.Env + "-" + corpConfig.WebDomainPrefix
	}

	// 实例aliyundns
	aliyunDns, err := aliyun.NewClient(c.Configure.Aliyun)
	if err != nil {
		return err
	}

	// 添加租户域名
	_, err = repositorie_tenancy.AddDomain(c, corpConfig.DomainPrefix+"."+c.Configure.Aliyun.DomainName, corp.TenantId)
	if err != nil {
		return err
	}

	// 企业配置不存在时将后端域名解析到阿里云
	if corpConfig.RecordId != "" {
		domainResult, err := aliyunDns.CreateDomainRecord(&corpConfig.DomainPrefix)
		if err != nil {
			return err
		}
		corpConfig.RecordId = *domainResult.Body.RecordId
	}

	// 企业配置不存在时将前端域名解析到阿里云
	if corpConfig.FrontRecordId != "" {
		webDomainResult, err := aliyunDns.CreateDomainRecord(&corpConfig.WebDomainPrefix)
		if err != nil {
			return err
		}
		corpConfig.FrontRecordId = *webDomainResult.Body.RecordId
	}
	config, err := json.Marshal(corpConfig)
	if err != nil {
		return err
	}
	corp.Config = string(config)

	// 把租户状态更改为开户完成
	corp.Status = model_tenancy.TENANT_CORP_STATUS_DISABLE
	return repositorie_tenancy.CorpUpdateById(c, corp.Id, corp)
}

//
//func NewOpenTenant(db *gorm.DB, conf *configure.Configure, logger *zap.SugaredLogger, tenantId string, corpName string, mobile string) *OpenTenant {
//	openTenant := &OpenTenant{
//		DB:        db,
//		Logger:    logger,
//		Configure: conf,
//		TenantId:  tenantId,
//		CorpName:  corpName,
//		Mobile:    mobile,
//	}
//	openTenant.nowTenantDb = databases.RegisterDb(openTenant.nowTenantDb, conf.Debug, conf.Database.IdlePool, conf.Database.Pool, logger, fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=utf8mb4,utf8", conf.Database.Username, conf.Database.Password, conf.Database.Host, conf.Database.Port, ""))
//	return openTenant
//}
