package queue_teancny

import (
	"encoding/json"
	"fmt"
	"gin/core"
	"gin/core/amqp"
	"gin/core/amqp/queue/interface"
	service_tenancy "gin/domain/tenancy/services"
)

// OpenCorp // 开户队列
type OpenCorp struct {
	Id uint
}

func (oc OpenCorp) Run() queue_interface.QueueHandleFunc {
	return func(c *core.Core, args ...string) error {
		config := amqp.PublishConfig{
			QueueName: amqp.TenancyOpenCorpQueue,
		}
		queue_interface.ConsumeSimple(c, config, oc.ConsumeFunc())
		return nil
	}
}

func (oc OpenCorp) ConsumeFunc() queue_interface.ConsumeSimpleFunc {
	return func(c *core.Core, content amqp.Content) error {
		contents, _ := json.Marshal(content.Content)
		dContent := new(OpenCorp)
		err := json.Unmarshal(contents, dContent)
		if err != nil {
			c.Logger.Error(fmt.Sprintf("json.Marshal err:%s", err))
		}
		return service_tenancy.InitCorp(c, dContent.Id)
	}
}
