package TenancyController

import (
	"gin/core"
	"gin/core/help"
	model_tenancy "gin/domain/tenancy/models"
	service_tenancy "gin/domain/tenancy/services"
	"github.com/gin-gonic/gin"
	"net/http"
)

// OpenCorp // 开户
func OpenCorp(c *core.Core) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		tenantCorp := model_tenancy.TenantCorp{}
		if err := ctx.ShouldBindJSON(&tenantCorp); err != nil {
			help.Error(ctx, http.StatusBadRequest, err.Error())
			return
		}
		_, err := service_tenancy.OpenCorp(c, &tenantCorp)
		if err != nil {
			help.Error(ctx, http.StatusBadRequest, err.Error())
			return
		}
		help.Success(ctx, tenantCorp)
	}
}
