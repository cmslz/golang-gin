package platform_tenancy

import (
	"gin/core"
	"gin/domain/tenancy/ports/platform/controllers"
	"github.com/gin-gonic/gin"
)

func Router(c *core.Core, r *gin.RouterGroup) {
	platform := r.Group("/platform")
	{
		platform.POST("/corp/open", TenancyController.OpenCorp(c))
	}
}
