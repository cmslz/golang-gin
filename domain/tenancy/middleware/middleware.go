package middleware_tenancy

import (
	"gin/core"
	"gin/core/help"
	"gin/domain/tenancy/repositories"
	"gin/domain/tenancy/services"
	"github.com/gin-gonic/gin"
)

func MiddleWare(c *core.Core) gin.HandlerFunc {
	return func(i *gin.Context) {
		c.GinContext = i
		TenancyId := TenancyId(c, i, "")
		// 重新注册各种数据库链接
		c = service_tenancy.Init(c, TenancyId)
		c.TenancyId = TenancyId
	}
}

// TenancyId // 获取租户id
func TenancyId(c *core.Core, ic *gin.Context, tenancyId string) string {
	if tenancyId == "" {
		domain := ic.Request.Host
		if domain != "" && c.Configure.CentralDomain != domain {
			tenancyId, err := repositorie_tenancy.TenancyIdByDomain(c, domain)
			if err != nil {
				help.Error(ic, 404, "页面不存在")
				ic.Next()
			}
			return tenancyId
		} else {
			tenancyId := ic.Param("tenancy_id")
			if tenancyId == "" {
				tenancyId = ic.GetHeader("x-tenant-id")
			}
		}
	}
	return tenancyId
}
