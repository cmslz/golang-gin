##海报合成服务

本服务仅提供 图片、文本 进行定位合成海报
#### 请求参数
|字段|类型|是否必填|备注|
|----|----|----|----|
|type|string|是|类型：image 图片；text 文本|
|url|string|否|图片地址：type为image时必填|
|content|string|否|文本内容：type为text时必填|
|width|int|是|宽度|
|height|int|是|高度|
|x|int|是|元素所在位置x轴|
|y|int|是|元素所在位置y轴|
|zIndex|int|是|元素所在层级|
|borderRadius|int|否|图片圆角:type为image时有效|
|lineHeight|int|否|文本行高:type为text时有效，最小值为1|
|bold|int|否|文本加粗值:type为text时有效，分别有:0,200,400,700,900|
|wrap|bool|否|文本自动换行:type为text时有效|
|fontSize|float|否|文本大小:type为text时有效、必填|
|fontFamily|string|否|文字字体:type为text时有效、目前仅支持 微软雅黑|
|color|string|否|文字颜色:type为text时有效、必填|
|align|string|否|文字对齐方式:type为text时有效 center、left、right|
|del|bool|否|文字删除线:type为text时有效|