package service_poster

// 叠加层
type Mask struct {
	Type         string  `json:"type"`
	Classify     string  `json:"classify"`
	Url          string  `json:"url"`
	X            float64 `json:"x"`
	Y            float64 `json:"y"`
	Width        float64 `json:"Width"`
	Height       float64 `json:"Height"`
	ZIndex       int     `json:"zIndex"`
	Alpha        float32 `json:"alpha"`
	BorderRadius int     `json:"borderRadius"`
	Content      string  `json:"content"`
	FontFamily   string  `json:"fontFamily"`
	FontSize     int     `json:"fontSize"`
	FontWeight   string  `json:"fontWeight"`
	Color        string  `json:"color"`
	Bold         int     `json:"bold"`
	Align        string  `json:"align"`
	Wrap         bool    `json:"wrap"`
	LineHeight   float64 `json:"lineHeight"`
	Del          bool    `json:"del"`
}

// 排序
type Masks []Mask

func (p Masks) Len() int {
	return len(p)
}

func (p Masks) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p Masks) Less(i, j int) bool {
	return p[i].ZIndex < p[j].ZIndex
}
