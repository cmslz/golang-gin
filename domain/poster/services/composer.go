package service_poster

import (
	"encoding/json"
	"fmt"
	"gin/core/cache"
	"gin/core/pool"
	"go.uber.org/zap"
	"gopkg.in/gographics/imagick.v3/imagick"
	"sort"
	"strings"
	"sync"
	"time"
)

const (
	FormatPng  = "png"
	FormatJpeg = "jpeg"
	//FormatJpg  = "jpg"
)

//Composer 数据合成器
type Composer struct {
	// 缩放倍数
	Scale int
	// 原始JSON数据
	RawData string
	// 风格
	Masks Masks
	// 图片数据
	DataMap map[string][]byte
	// 下载时间
	DownTime time.Duration
	// 合成时间
	CompositeTime time.Duration
	// 底图格式
	Format string
	Width  int
	Height int
	Cache  *cache.Cache
	Logger *zap.SugaredLogger
}

// NewComposer // 实例化合成器
func NewComposer(c *cache.Cache, l *zap.SugaredLogger, rawData string, scale int, width int, height int, format string) *Composer {
	return &Composer{
		RawData: rawData,
		Masks:   make([]Mask, 0),
		DataMap: make(map[string][]byte),
		Scale:   scale,
		Width:   width,
		Height:  height,
		Format:  format,
		Cache:   c,
		Logger:  l,
	}
}

//func (m *Composer) Composite() ([]byte, error) {
func (m *Composer) Composite() ([]byte, error) {
	if err := m.decodeRawData(); err != nil {
		return nil, err
		//return nil, err
	}
	if m.Width == 0 {
		m.Width = int(m.Masks[0].Width)
		m.Height = int(m.Masks[0].Height)
	}
	for i := range m.Masks {
		m.Masks[i].X *= float64(m.Scale)
		m.Masks[i].Y *= float64(m.Scale)
		m.Masks[i].Width *= float64(m.Scale)
		m.Masks[i].Height *= float64(m.Scale)
		m.Masks[i].BorderRadius *= m.Scale
		m.Masks[i].FontSize *= m.Scale
	}
	if err := m.downloadData(); err != nil {
		//return nil, err
		return nil, err
	}
	start := time.Now()
	canvas := m.drawCanvas()
	defer canvas.Destroy()

	sort.Sort(m.Masks)
	painter := ImagickPainter{
		Cache:  m.Cache,
		Logger: m.Logger,
	}

	for _, item := range m.Masks {
		err := m.paint(canvas, &item, painter)
		if err != nil {
			return nil, err
			//return nil, err
		}
	}

	m.CompositeTime = time.Since(start)
	return canvas.GetImageBlob(), nil
}

// paint 绘制
func (m *Composer) paint(canvas *imagick.MagickWand, meta *Mask, imagickPainter ImagickPainter) error {
	if meta.Type == "text" {
		return imagickPainter.drawText(canvas, meta)
	} else if meta.Type == "image" {
		if len(meta.Url) == 0 {
			return nil
		}
		return imagickPainter.drawImage(canvas, meta, m.DataMap[meta.Url])
	} else {
		m.Logger.Error("数据类型错误")
		return nil
	}
}

// decodeRawData 解析原始数据为JSON
func (m *Composer) decodeRawData() error {
	return json.NewDecoder(strings.NewReader(m.RawData)).Decode(&m.Masks)
}

// downloadData 下载图片数据
func (m *Composer) downloadData() error {
	var (
		p     = pool.New(5, len(m.Masks))
		mu    = &sync.Mutex{}
		err   error
		start = time.Now()
	)
	PoserCache := NewCache(m.Cache, m.Logger)
	for _, item := range m.Masks {
		item := item
		p.Submit(func() {
			if item.Url == "" {
				return
			}
			data, mErr := PoserCache.getCacheImgData(item.Url, 10)
			mu.Lock()
			defer mu.Unlock()
			if mErr != nil {
				err = mErr
				return
			}
			m.DataMap[item.Url] = data
		})
	}
	p.Wait()
	m.DownTime = time.Since(start)
	return err
}

// 绘制底图
func (m *Composer) drawCanvas() *imagick.MagickWand {
	mw := imagick.NewMagickWand()
	pw := imagick.NewPixelWand()
	defer pw.Destroy()

	switch m.Format {
	case FormatPng:
		pw.SetColor("none")
		err := mw.NewImage(uint(m.Width), uint(m.Height), pw)
		if err != nil {
			m.Logger.Error(fmt.Sprintf("mw.NewImage err:%s", err))
		}
		err = mw.SetImageFormat(FormatPng)
		if err != nil {
			m.Logger.Error(fmt.Sprintf("mw.SetImageFormat err:%s", err))
		}
		break
	default:
		pw.SetColor("white")
		err := mw.NewImage(uint(m.Width), uint(m.Height), pw)
		if err != nil {
			m.Logger.Error(fmt.Sprintf("mw.NewImage err:%s", err))
		}
		err = mw.SetImageFormat(FormatJpeg)
		if err != nil {
			m.Logger.Error(fmt.Sprintf("mw.SetImageFormat err:%s", err))
		}
		err = mw.SetCompression(imagick.COMPRESSION_JPEG)
		if err != nil {
			m.Logger.Error(fmt.Sprintf("mw.SetCompression err:%s", err))
		}
		err = mw.SetCompressionQuality(80)
		if err != nil {
			m.Logger.Error(fmt.Sprintf("mw.SetCompressionQuality err:%s", err))
		}
		break
	}

	return mw
}
