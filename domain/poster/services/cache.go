package service_poster

import (
	"fmt"
	"gin/core/cache"
	"gin/core/help"
	"gin/core/util/httputil"
	"go.uber.org/zap"
	"time"
)

type PoserCache struct {
	Cache  *cache.Cache
	Logger *zap.SugaredLogger
}

// NewCache // 创建缓存
func NewCache(c *cache.Cache, l *zap.SugaredLogger) PoserCache {
	return PoserCache{Cache: c, Logger: l}
}

func (p *PoserCache) getCacheImgData(url string, timeout time.Duration) ([]byte, error) {
	md5string := help.Md5("v2" + url)
	data, err := p.Cache.Get(md5string).Result()
	if err != nil {
		downloadData, err := httputil.Download(url, timeout)
		if err != nil {
			p.Logger.Error(fmt.Sprintf("[ Cache:getCacheImgData ] download Cache file error : %s", err))
			return nil, err
		}
		data = string(downloadData)
		p.Cache.Set(md5string, data, 0)
	}
	return []byte(data), nil
}
