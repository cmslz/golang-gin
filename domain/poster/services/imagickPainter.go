package service_poster

import (
	"encoding/json"
	"errors"
	"fmt"
	"gin/core/cache"
	"gin/core/util/imgutil"
	"gin/core/util/strutil"
	"go.uber.org/zap"
	"gopkg.in/gographics/imagick.v3/imagick"
	"os"
)

type ImagickPainter struct {
	Cache  *cache.Cache
	Logger *zap.SugaredLogger
}

// 绘制图片
func (m ImagickPainter) drawImage(canvas *imagick.MagickWand, meta *Mask, data []byte) error {
	var err error

	PoserCache := NewCache(m.Cache, m.Logger)
	if data == nil {
		data, err = PoserCache.getCacheImgData(meta.Url, 10)
		if err != nil {
			m.Logger.Info("[ImagickPainter::drawImage] download error: %s %s; continue", err, meta.Url)
			return err
		}
	}

	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	if err := mw.ReadImageBlob(data); err != nil {
		buf, _ := json.Marshal(meta)
		return errors.New(fmt.Sprintf("[ImagickPainter::drawImage] readImage error: %s %s %s; continue", err, meta.Url, string(buf)))
	}
	if err := mw.ResizeImage(uint(meta.Width), uint(meta.Height), imagick.FILTER_LANCZOS); err != nil {
		buf, _ := json.Marshal(meta)
		return errors.New(fmt.Sprintf("[ImagickPainter::drawImage] resizeImage error: %s %s %s; continue", err, meta.Url, string(buf)))
	}
	// 圆角处理
	if meta.BorderRadius > 0 {
		img := m.roundCorners(mw, float64(meta.BorderRadius))
		defer img.Destroy()

		return canvas.CompositeImage(img, imagick.COMPOSITE_OP_OVER, true, int(meta.X), int(meta.Y))
	}
	return canvas.CompositeImage(mw, imagick.COMPOSITE_OP_OVER, true, int(meta.X), int(meta.Y))
}

// 绘制文本
func (m ImagickPainter) drawText(canvas *imagick.MagickWand, meta *Mask) error {
	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	dw := imagick.NewDrawingWand()
	defer dw.Destroy()
	pw := imagick.NewPixelWand()
	defer pw.Destroy()

	pw.SetColor("none")
	if err := mw.NewImage(uint(meta.Width), uint(meta.Height), pw); err != nil {
		return err
	}
	pw.SetColor(meta.Color)
	dw.SetFillColor(pw)
	dw.SetStrokeAntialias(true)
	dw.SetTextAntialias(true)

	fontPath := "../../../assets/font"
	font := fontPath + "/SourceHanSansCN-Normal.otf"
	// 默认使用SourceHanSansCN，老数据大部分都是"微软雅黑" 做个兼容
	if meta.FontFamily == "" || meta.FontFamily == "微软雅黑" {
		font = fontPath + "/SourceHanSansCN-Normal.otf"
		if meta.Bold == 0 && meta.FontWeight == "bold" {
			meta.Bold = 400
		}
		if meta.Bold >= 900 {
			font = fontPath + "/SourceHanSansCN-Heavy.otf"
		} else if meta.Bold >= 700 {
			font = fontPath + "/SourceHanSansCN-Bold.otf"
		} else if meta.Bold >= 400 {
			font = fontPath + "/SourceHanSansCN-Medium.otf"
		} else if meta.Bold >= 200 {
			font = fontPath + "/SourceHanSansCN-Regular.otf"
		} else if meta.Bold > 0 {
			font = fontPath + "/SourceHanSansCN-Light.otf"
		}
	} else {
		font = fontPath + "/" + meta.FontFamily
	}
	if !m.isExists(font) {
		font = fontPath + "/SourceHanSansCN-Normal.otf"
	}

	if err := dw.SetFont(font); err != nil {
		return err
	}
	fontSize := float64(meta.FontSize)

	dw.SetFontSize(fontSize)
	// 居中处理
	switch meta.Align {
	case "center":
		dw.SetGravity(imagick.GRAVITY_NORTH)
		break
	case "right":
		dw.SetGravity(imagick.GRAVITY_NORTH_EAST)
		break
	case "left":
		dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
		break
	default:
		dw.SetGravity(imagick.GRAVITY_NORTH_WEST)
		break
	}

	content := strutil.AutoWrap(int(meta.Width), meta.Content, meta.Wrap, dw, mw)
	// 换行处理

	lineCount := len(content)
	// 行高最小为1
	if meta.LineHeight < 1 {
		meta.LineHeight = 1
	}

	lineHeight := fontSize * meta.LineHeight

	imageHeight := (fontSize + lineHeight + 2) * float64(lineCount+1)

	pw.SetColor("none")

	err := mw.NewImage(uint(meta.Width), uint(imageHeight), pw)
	if err != nil {
		return err
	}
	pw.SetColor(meta.Color)
	dw.SetFillColor(pw)

	for idx, row := range content {
		info := mw.QueryFontMetrics(dw, row)
		yOffset := float64(idx)*lineHeight + float64((lineHeight-info.CharacterHeight)/2)
		dw.Annotation(0, yOffset, row)
		if meta.Del {
			err := imgutil.DrawLine(canvas, info.TextWidth, info.TextHeight, int(meta.X), int(meta.Y), meta.Color)
			if err != nil {
				m.Logger.Info(" Del line draw failed : %s", err)
			}
		}
	}
	if err := mw.DrawImage(dw); err != nil {
		return err
	}
	if err := canvas.CompositeImage(mw, imagick.COMPOSITE_OP_OVER, true, int(meta.X), int(meta.Y)); err != nil {
		return err
	}
	return nil
}

func (ImagickPainter) isExists(str string) bool {
	_, err := os.Stat(str)
	if err == nil {
		return true
	}
	return false
}

func (ImagickPainter) roundCorners(img *imagick.MagickWand, borderRadius float64) *imagick.MagickWand {
	var (
		mw = imagick.NewMagickWand()
		pw = imagick.NewPixelWand()
		dw = imagick.NewDrawingWand()
	)
	defer pw.Destroy()
	defer dw.Destroy()

	pw.SetColor("none")
	_ = mw.NewImage(img.GetImageWidth(), img.GetImageHeight(), pw)

	pw.SetColor("white")
	dw.SetFillColor(pw)
	dw.RoundRectangle(0, 0, float64(img.GetImageWidth()), float64(img.GetImageHeight()), borderRadius, borderRadius)
	_ = mw.DrawImage(dw)
	_ = mw.CompositeImage(img, imagick.COMPOSITE_OP_SRC_IN, true, 0, 0)
	return mw
}
