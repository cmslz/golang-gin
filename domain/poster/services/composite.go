package service_poster

import (
	"gin/core/cache"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"io/ioutil"
	"strconv"
)

const (
	MAXQUEUE = 10 // 最大10条队列同时进行，超过则阻塞
)

func New(c *cache.Cache, l *zap.SugaredLogger) gin.HandlerFunc {
	ch := make(chan struct{}, MAXQUEUE)
	return func(ctx *gin.Context) {
		ch <- struct{}{}
		defer func() { <-ch }()

		strFormat := ctx.DefaultQuery("format", "jpeg")
		strScale := ctx.DefaultQuery("scale", "1")
		strWidth := ctx.DefaultQuery("width", "0")
		strHeight := ctx.DefaultQuery("height", "0")
		scale, _ := strconv.ParseFloat(strScale, 64)
		width, _ := strconv.ParseFloat(strWidth, 64)
		height, _ := strconv.ParseFloat(strHeight, 64)
		data, err := ioutil.ReadAll(ctx.Request.Body)
		if err != nil {
			ctx.JSON(200, gin.H{"errmsg": err.Error(), "errcode": 400})
			return
		}

		composer := NewComposer(c, l, string(data), int(scale), int(width), int(height), strFormat)

		img, err := composer.Composite()
		if err != nil {
			ctx.JSON(200, gin.H{"errmsg": err.Error(), "errcode": 500})
			return
		}

		ctx.Data(200, "image/jpeg", img)
		return
	}
}
