package PosterController

import (
	"fmt"
	"gin/core"
	"gin/core/help"
	"gin/domain/poster/services"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
)

const (
	MAXQUEUE = 1 // 最大10条队列同时进行，超过则阻塞
)

func Composite(c *core.Core) gin.HandlerFunc {
	ch := make(chan struct{}, MAXQUEUE)
	return func(ctx *gin.Context) {
		ch <- struct{}{}
		defer func() { <-ch }()

		strFormat := ctx.DefaultQuery("format", "jpeg")
		strScale := ctx.DefaultQuery("scale", "1")
		strWidth := ctx.DefaultQuery("width", "0")
		strHeight := ctx.DefaultQuery("height", "0")
		scale, _ := strconv.ParseFloat(strScale, 64)
		width, _ := strconv.ParseFloat(strWidth, 64)
		height, _ := strconv.ParseFloat(strHeight, 64)
		data, err := ioutil.ReadAll(ctx.Request.Body)
		if err != nil {
			help.Error(ctx, http.StatusInternalServerError, fmt.Sprintf("ReadAll err:%s", err))
			return
		}
		composer := service_poster.NewComposer(c.Cache, c.Logger, string(data), int(scale), int(width), int(height), strFormat)

		img, err := composer.Composite()
		if err != nil {
			help.Error(ctx, http.StatusInternalServerError, fmt.Sprintf("Composite err:%s", err))
			return
		}
		ctx.Data(200, "image/jpeg", img)
		return
	}
}
