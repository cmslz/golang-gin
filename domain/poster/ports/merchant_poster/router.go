package merchant_poster

import (
	"gin/core"
	"gin/domain/poster/ports/merchant_poster/controllers"
	"github.com/gin-gonic/gin"
)

func Router(c *core.Core, r *gin.RouterGroup) {
	corp := r.Group("/merchant")
	{
		corp.POST("/composite", PosterController.Composite(c))
	}
}
