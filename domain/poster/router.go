package router_poser

import (
	"gin/core"
	"gin/domain/poster/ports/merchant_poster"
	"gin/domain/tenancy/middleware"
)

func Router(c *core.Core) {
	c.Engine.Use(middleware_tenancy.MiddleWare(c))
	poster := c.Engine.Group("/poster")
	{
		merchant_poster.Router(c, poster)
	}
}
