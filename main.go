package main

import (
	"gin/core"
	"gin/router"
)

func main() {
	// 初始化核心
	c := core.New(nil)
	// 增加路由分发
	Hooks := make([]core.Hook, 0)

	Hooks = append(Hooks, router.Router)
	//
	//if c.Configure.Debug {
	//	Hooks = append(Hooks, gii.Hook)
	//}
	// 追加路由
	c.AddHooks(Hooks...)
	// 启动
	c.Run()
}
