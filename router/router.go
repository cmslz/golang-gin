package router

import (
	"gin/core"
	"gin/domain/corp"
	"gin/domain/poster"
	"gin/domain/tenancy"
)

func Router(c *core.Core) {
	router_corp.Router(c)
	router_poser.Router(c)
	touter_tenancy.Router(c)
}
