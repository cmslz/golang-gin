package core

import (
	"context"
	"fmt"
	"gin/core/amqp"
	"gin/core/cache"
	"gin/core/configure"
	"gin/core/databases"
	"gin/core/help"
	"gin/core/logger"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/olivere/elastic/v7"
	"go.uber.org/zap"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"syscall"
)

type Core struct {
	Configure          *configure.Configure
	params             Params
	beforeRun          []BeforeHook
	beforeShutdown     []AfterHook
	ctx                context.Context
	cancels            []context.CancelFunc
	Db                 *gorm.DB
	Engine             *gin.Engine
	StartHooks         []Hook
	GinContext         *gin.Context
	ElasticSearch      *elastic.Client
	Logger             *zap.SugaredLogger
	Cache              *cache.Cache
	RabbitMQ           *amqp.AmqpData
	RedisClient        *redis.Client
	TenancyDb          *gorm.DB
	TenancyId          string `xml:"tenancy_id" json:"tenancy_id" yaml:"tenancy_id"`
	TenancyRedisClient *redis.Client
	TenancyCache       *cache.Cache
}

const PidFile = "gin.pid"

type Hook func(*Core)

type BeforeHook func(*Core, context.Context)

type AfterHook func(*Core) error
type Params map[string]interface{}

func New(config *configure.Configure) *Core {
	if config == nil {
		config = configure.New()
	}
	core := &Core{
		Configure: config,
		params:    Params{},
		ctx:       context.Background(),
		Engine:    gin.Default(),
		TenancyDb: nil,
	}
	ports := strings.Split(os.Getenv("PORT"), ",")
	if os.Getenv("PORT") != "" && len(ports) > 0 {
		core.Configure.Port = ports
	}

	return core
}

func (c *Core) Run() {
	c.Init()
	// 启动
	if c.Configure.Daemon {
		c.start()
	} else {
		c.start()
	}
}

// Init // 初始化注册
func (c *Core) Init() {
	c.registerElastic().registerLogger().registerDb().registerRedis().registerRabbitMQ().useHooks()
}

// start /** 服务启动
func (c *Core) start() {
	for _, h := range c.beforeRun {
		if h != nil {
			ctx, cancel := context.WithCancel(c.ctx)
			h(c, ctx)
			c.cancels = append(c.cancels, cancel)
		}
	}

	ports := c.Configure.Port[:1]
	//servers := make([]*http.Server, len(ports))
	//for i, p := range ports {
	//	if p != "" {
	//		servers[i] = &http.Server{Addr: fmt.Sprintf(":%s", p), Handler: c.Engine}
	//	}
	//}

	if pid := syscall.Getpid(); pid != 1 {
		_ = ioutil.WriteFile(PidFile, []byte(strconv.Itoa(pid)), 0655)
	}

	log.Println("启动端口: ", ports)

	err := c.Engine.Run(fmt.Sprintf(":%s", ports[0]))
	if err != nil {
		c.Logger.Fatalf(fmt.Sprintf("ListenAndServe err:%s", err))
	}
	defer c.HandleDeferFunc(c.Shutdown)
}

func (c *Core) AddHooks(dispatches ...Hook) *Core {
	c.StartHooks = dispatches
	return c
}

func (c *Core) useHooks() *Core {
	if len(c.StartHooks) != 0 {
		for _, r := range c.StartHooks {
			r(c)
		}
	}
	return c
}

func (c *Core) Shutdown() error {
	// before shutdown
	c.Logger.Info("shutdown")
	for _, h := range c.beforeShutdown {
		if err := h(c); err != nil {
			return err
		}
	}

	for _, cancel := range c.cancels {
		cancel()
	}

	log.Println("server has been shutdown")
	if err := c.Db.Close(); err != nil {
		return err
	}

	if help.Exists(PidFile) {
		if err := os.Remove(PidFile); err != nil {
			return err
		}
	}

	return nil
}

// HandleDeferFunc /** 处理延迟方法
func (c *Core) HandleDeferFunc(f func() error) {
	err := f()
	if err != nil {
		c.Logger.Fatalf("defer hanled error %s", err)
	}
}

// registerDb /** 注册db
func (c *Core) registerDb() *Core {
	mysqlConn := fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=%s", c.Configure.Database.Username, c.Configure.Database.Password, c.Configure.Database.Host, c.Configure.Database.Port, c.Configure.Database.DbName, c.Configure.Database.Charset)
	c.Db = databases.RegisterDb(c.Db, c.Configure.Debug, c.Configure.Database.IdlePool, c.Configure.Database.Pool, c.Logger, mysqlConn)
	return c
}

// registerElastic // 注册es
func (c *Core) registerElastic() *Core {
	client, err := elastic.NewClient(elastic.SetURL(c.Configure.ElasticSearch.Host), elastic.SetBasicAuth(c.Configure.ElasticSearch.User, c.Configure.ElasticSearch.Password))
	if err != nil {
		// Handle error
		log.Fatalf(fmt.Sprintf("registerElastic err:%s", err))
	}
	c.ElasticSearch = client
	log.Println("elastic client success")
	return c
}

// registerLogger // 注册日志
func (c *Core) registerLogger() *Core {
	c.Logger = logger.RegisterLogger(c.Logger, c.Configure, c.ElasticSearch, c.TenancyId)
	return c
}

// registerRedis // 注册redis
func (c *Core) registerRedis() *Core {
	c.RedisClient, c.Cache = cache.RegisterRedis(c.RedisClient, c.Cache, c.Configure, c.TenancyId)
	return c
}

// registerRabbitMQ // 注册RabbitMQ
func (c *Core) registerRabbitMQ() *Core {
	c.RabbitMQ = amqp.NewRabbitMQSimple(c.Configure, c.Logger)
	return c
}
