package cache

import (
	"fmt"
	"gin/core/help"
	"github.com/go-redis/redis"
	"strconv"
	"strings"
	"time"
)

type Cache struct {
	redisClient *redis.Client
	Prefix      string `json:"prefix" yaml:"prefix" xml:"prefix"`
	Version     string `json:"version" yaml:"version" xml:"version"`
	TenancyId   string `json:"tenancy_id" yaml:"tenancy_id" xml:"tenancy_id"`
}

func New(r *redis.Client, prefix string, version string, tenancyId string) *Cache {
	return &Cache{
		redisClient: r,
		Prefix:      tenancyId + "_" + prefix,
		Version:     version,
		TenancyId:   tenancyId,
	}
}

func (c *Cache) HSet(key string, field string, v interface{}, duration time.Duration) error {
	if duration > 0 {
		duration = time.Duration(time.Now().Unix())*time.Second + duration
		if err := c.redisClient.HSet(c.buildDurationKey(key), field, strconv.FormatInt(int64(duration), 10)).Err(); err != nil {
			return err
		}
	}
	return c.redisClient.HSet(c.BuildKey(key), field, v).Err()
}

func (c *Cache) HGet(key string, field string) *redis.StringCmd {
	duration, _ := c.redisClient.HGet(c.buildDurationKey(key), field).Int64()
	if duration > 0 && time.Duration(duration) < time.Duration(time.Now().Unix())*time.Second {
		c.redisClient.HDel(c.BuildKey(key), field)
	}
	return c.redisClient.HGet(c.BuildKey(key), field)
}

func (c *Cache) HDel(key string, field ...string) bool {
	return c.redisClient.HDel(c.BuildKey(key), field...).Val() == int64(len(field))
}

func (c *Cache) HExists(key string, field string) bool {
	return c.redisClient.HExists(key, field).Val()
}

func (c *Cache) Del(key ...string) bool {
	res := c.redisClient.Del(c.BuildKeys(key)...).Val()
	return res == int64(len(key))
}

func (c *Cache) Set(key string, value interface{}, duration time.Duration) *redis.StatusCmd {
	return c.redisClient.Set(c.BuildKey(key), value, duration)
}

func (c *Cache) Get(key string) *redis.StringCmd {
	return c.redisClient.Get(c.BuildKey(key))
}

func (c *Cache) LPush(key string, v interface{}) *redis.IntCmd {
	return c.redisClient.LPush(c.BuildKey(key), v)
}

func (c *Cache) RPush(key string, v interface{}) *redis.IntCmd {
	return c.redisClient.LPush(c.BuildKey(key), v)
}
func (c *Cache) BLPop(key string, timeout time.Duration) string {
	res := c.redisClient.BLPop(timeout, c.BuildKey(key)).Val()
	if len(res) == 0 {
		return ""
	}
	return res[len(res)-1]
}

func (c *Cache) BRPop(key string, timeout time.Duration) string {
	res := c.redisClient.BRPop(timeout, c.BuildKey(key)).Val()
	if len(res) == 0 {
		return ""
	}
	return res[len(res)-1]
}

func (c *Cache) LLen(key string) *redis.IntCmd {
	return c.redisClient.LLen(c.BuildKey(key))
}

func (c *Cache) Exists(key ...string) *redis.IntCmd {
	return c.redisClient.Exists(c.BuildKeys(key)...)
}

func (c *Cache) BuildKeys(keys []string) []string {
	buildFields := make([]string, len(keys))
	for _, f := range keys {
		buildFields = append(buildFields, c.BuildKey(f))
	}
	return buildFields
}

func (c *Cache) BuildKey(strs ...string) string {
	newStrs := make([]string, len(strs)+2)
	// add Prefix to first
	newStrs = append(newStrs, c.Prefix, fmt.Sprintf(":%s", c.Version))
	// add all keys
	newStrs = append(newStrs, strs...)
	// make keys to string
	str := strings.Join(newStrs, ":")
	// md5 str
	return help.Md5(str)
}

func (c *Cache) buildDurationKey(strs ...string) string {
	strs = append(strs, "duration")
	return c.BuildKey(strs...)
}
