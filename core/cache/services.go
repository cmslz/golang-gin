package cache

import (
	"fmt"
	"gin/core/configure"
	"github.com/go-redis/redis"
	"time"
)

// RegisterRedis // 注册redis
func RegisterRedis(r *redis.Client, ca *Cache, cf *configure.Configure, tenancyId string) (*redis.Client, *Cache) {
	redisOptions := cf.Redis

	client := redis.NewClient(&redis.Options{
		Addr:        fmt.Sprintf("%s:%d", redisOptions.Host, redisOptions.Port),
		Password:    redisOptions.Password,
		DB:          0,
		PoolSize:    redisOptions.Pool,
		PoolTimeout: time.Second * 3,
	})
	r = client

	ca = New(r, redisOptions.Prefix, cf.Version, tenancyId)
	return r, ca
}
