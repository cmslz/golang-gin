package configure

type Database struct {
	Host      string `yaml:"host"`
	Port      int    `yaml:"port"`
	Username  string `yaml:"username"`
	DbName    string `yaml:"dbName"`
	Password  string `yaml:"password"`
	Pool      int    `yaml:"pool"`
	IdlePool  int    `yaml:"idle_pool"`
	Charset   string `yaml:"charset"`
	Collation string `yaml:"collation"`
}

type ElasticSearch struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

type Log struct {
	Driver         string `yaml:"driver"`
	Name           string `yaml:"name"`
	LowStartLevel  int    `yaml:"low_start_level"`
	HighStartLevel int    `yaml:"high_start_level"`
}

type Redis struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Prefix   string `yaml:"prefix"`
	Password string `yaml:"password"`
	Pool     int    `yaml:"pool"`
}

type Amqp struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Vhost    string `yaml:"vhost"`
}
