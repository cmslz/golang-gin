package configure

import (
	model_aliyun "gin/core/aliyun/models"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

var configFilePath = "./config/env.yml"

type Configure struct {
	AppName       string               `yaml:"app_name"`
	NodeName      string               `yaml:"node_name"`
	Port          []string             `yaml:"port"`
	LocalDomain   string               `yaml:"local_domain"`
	CentralDomain string               `yaml:"central_domain"`
	Env           string               `yaml:"env"`
	Debug         bool                 `yaml:"debug"`
	Daemon        bool                 `yaml:"daemon"`
	Version       string               `yaml:"version"`
	Database      *Database            `yaml:"database"`
	ElasticSearch *ElasticSearch       `yaml:"elastic_search"`
	Log           *Log                 `yaml:"log"`
	Redis         *Redis               `yaml:"redis"`
	Amqp          *Amqp                `yaml:"amqp"`
	Aliyun        *model_aliyun.Aliyun `yaml:"aliyun"`
}

func New() *Configure {
	c, err := DecodeYaml(configFilePath)
	if err != nil {
		log.Fatalf("yaml unmarshal failed %s", err)
	}
	return c
}

func DecodeYaml(path string) (*Configure, error) {
	log.Printf("loaded config file with %s", path)
	c := &Configure{}
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(bytes, c)
	if err != nil {
		return nil, err
	}

	return c, nil
}
