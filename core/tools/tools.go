package tools

import (
	"time"
)

func TimeLocation() *time.Location {
	return time.FixedZone("CST", 8*3600)
}

func Now() time.Time {
	return time.Now().In(TimeLocation())
}
