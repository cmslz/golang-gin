package amqp

import (
	"github.com/streadway/amqp"
)

// AmqpData //AmqpData结构体
type AmqpData struct {
	Connection *amqp.Connection
	Channel    *amqp.Channel
}

// PublishConfig // Publish配置
type PublishConfig struct {
	QueueName  string     // 队列
	Exchange   string     // 交换机
	Durable    bool       // 是否持久化
	AutoDelete bool       // 是否自动删除
	Exclusive  bool       // 是否具有排他性
	OnWait     bool       // 是否阻塞处理
	Args       amqp.Table // 额外的属性
	Mandatory  bool       // 如果为true，根据自身exchange类型和routekey规则无法找到符合条件的队列会把消息返还给发送者
	Immediate  bool       // 如果为true，当exchange发送消息到队列后发现队列上没有消费者，则会把消息返还给发送者
	Content    Content    // 内容
}

type Content struct {
	TenancyId string
	Content   interface{}
}
