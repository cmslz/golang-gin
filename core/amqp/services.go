package amqp

import (
	"encoding/json"
	"fmt"
	"gin/core/configure"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

// Destory // 断开channel 和 connection
func (a *AmqpData) Destory(l *zap.SugaredLogger) {
	err := a.Channel.Close()
	if err != nil {
		l.Fatalf(fmt.Sprintf("Destory AmqpData Channel err:%s", err))
	}
	err = a.Connection.Close()
	if err != nil {
		l.Fatalf(fmt.Sprintf("Destory AmqpData Connection err:%s", err))
	}
}

// NewRabbitMQSimple // 创建简单模式下RabbitMQ实例
func NewRabbitMQSimple(cf *configure.Configure, l *zap.SugaredLogger) *AmqpData {
	amqpConn := fmt.Sprintf("amqp://%s:%s@%s:%d/%s", cf.Amqp.Username, cf.Amqp.Password, cf.Amqp.Host, cf.Amqp.Port, cf.Amqp.Vhost)
	conn, err := amqp.Dial(amqpConn)
	if err != nil {
		l.Fatalf(fmt.Sprintf("获取connection buildAmqpConn:%s err:%s", amqpConn, err))
	}
	//l.Fatalf(fmt.Sprintf("sadasda %s", conn))
	//获取channel
	channel, err := conn.Channel()
	if err != nil {
		l.Fatalf(fmt.Sprintf("获取channel err:%s", err))
	}

	//mq.Connection = conn
	//err = rabbitmq.Channel.ExchangeDeclare("e1", "direct", true, false, false, true, nil)
	//
	//if err != nil {
	//	l.Fatalf(fmt.Sprintf("获取channel err:%s", err))
	//}
	return &AmqpData{
		Connection: conn,
		Channel:    channel,
	}
}

//PublishSimple //直接模式队列生产
func PublishSimple(l *zap.SugaredLogger, a *AmqpData, config PublishConfig) error {
	//1.申请队列，如果队列不存在会自动创建，存在则跳过创建
	_, err := a.Channel.QueueDeclare(
		config.QueueName,
		//是否持久化
		config.Durable,
		//是否自动删除
		config.AutoDelete,
		//是否具有排他性
		config.Exclusive,
		//是否阻塞处理
		config.OnWait,
		//额外的属性
		config.Args,
	)
	if err != nil {
		l.Error(fmt.Sprintf("申请队列 err:%s", err))
		return err
	}

	content, _ := json.Marshal(&config.Content)
	//调用channel 发送消息到队列中
	err = a.Channel.Publish(
		config.Exchange,
		config.QueueName,
		//如果为true，根据自身exchange类型和routekey规则无法找到符合条件的队列会把消息返还给发送者
		config.Mandatory,
		//如果为true，当exchange发送消息到队列后发现队列上没有消费者，则会把消息返还给发送者
		config.Immediate,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        content,
		})
	if err != nil {
		l.Error(fmt.Sprintf("调用channel 发送消息到队列中 err:%s", err))
	}
	return err
}
