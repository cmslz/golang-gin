package queue_interface

import (
	"encoding/json"
	"fmt"
	"gin/core"
	"gin/core/amqp"
	"gin/domain/tenancy/services"
)

type QueueHandleFunc func(c *core.Core, args ...string) error
type ConsumeSimpleFunc func(c *core.Core, content amqp.Content) error
type QueueInterface interface {
	Run() QueueHandleFunc
	ConsumeFunc() ConsumeSimpleFunc
}

//ConsumeSimple 模式下消费者
func ConsumeSimple(c *core.Core, config amqp.PublishConfig, fun ConsumeSimpleFunc) {
	//1.申请队列，如果队列不存在会自动创建，存在则跳过创建
	q, err := c.RabbitMQ.Channel.QueueDeclare(
		config.QueueName,
		//是否持久化
		config.Durable,
		//是否自动删除
		config.AutoDelete,
		//是否具有排他性
		config.Exclusive,
		//是否阻塞处理
		config.OnWait,
		//额外的属性
		config.Args,
	)
	if err != nil {
		c.Logger.Error(fmt.Sprintf("ConsumeSimple 申请队列 err:%s", err))
	}

	//接收消息
	msgs, err := c.RabbitMQ.Channel.Consume(
		q.Name, // queue
		//用来区分多个消费者
		"", // consumer
		//是否自动应答
		true, // auto-ack
		//是否独有
		false, // exclusive
		//设置为true，表示 不能将同一个Conenction中生产者发送的消息传递给这个Connection中 的消费者
		false, // no-local
		//列是否阻塞
		false, // no-wait
		nil,   // Args
	)
	if err != nil {
		c.Logger.Error(fmt.Sprintf("接收消息 err:%s", err))
	}

	forever := make(chan bool)
	//启用协程处理消息
	go func() {
		for d := range msgs {
			content := new(amqp.Content)
			err := json.Unmarshal(d.Body, content)
			//var content Content

			//content, err := json.Marshal(d.Body)
			if err != nil {
				c.Logger.Error(fmt.Sprintf("queueName:%s json.Marshal err:%s", config.QueueName, err))
			}
			//消息逻辑处理，可以自行设计逻辑
			//log.Printf("Received a tenancyId: %s content:%s", content.TenancyId, content.Content)
			if content.TenancyId != "" {
				service_tenancy.Init(c, content.TenancyId)
				c.TenancyId = content.TenancyId
			}
			err = fun(c, amqp.Content{TenancyId: content.TenancyId, Content: content.Content})
			//content, err := json.Marshal(d.Body)
			if err != nil {
				c.Logger.Error(fmt.Sprintf("queueName:%s run fun err:%s", config.QueueName, err))
			}
		}
	}()

	c.Logger.Info(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
