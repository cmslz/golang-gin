package bin

import (
	"encoding/json"
	"fmt"
	"gin/core"
	"gin/core/amqp"
	"gin/core/amqp/queue/interface"
	"gin/domain/corp/repositories"
)

type Demo struct {
	Name string
	Id   uint
}

func (d Demo) Run() queue_interface.QueueHandleFunc {
	return func(c *core.Core, args ...string) error {
		config := amqp.PublishConfig{
			QueueName: "demo",
		}
		queue_interface.ConsumeSimple(c, config, d.ConsumeFunc())
		//c.Logger.Info("demo up")
		c.Logger.Info(fmt.Sprintf("queue args:%s", args))
		return nil
	}
}

func (d Demo) ConsumeFunc() queue_interface.ConsumeSimpleFunc {
	return func(c *core.Core, content amqp.Content) error {
		contents, _ := json.Marshal(content.Content)
		dContent := new(Demo)
		err := json.Unmarshal(contents, dContent)
		if err != nil {
			c.Logger.Error(fmt.Sprintf("json.Marshal err:%s", err))
		}
		c.Logger.Info(fmt.Sprintf("demo tenancyId:%s content.name:%s content.id:%d", content.TenancyId, dContent.Name, dContent.Id))
		cacheValue, err := c.Cache.Get("Hello").Result()

		if err != nil {
			c.Logger.Error(fmt.Sprintf("cacheValue err:%s", err))
		}
		c.Logger.Info(fmt.Sprintf("cacheValue:%s", cacheValue))
		TenancyCacheValue, err := c.TenancyCache.Get("HelloW").Result()

		if err != nil {
			c.Logger.Error(fmt.Sprintf("TenancyCacheValue err:%s", err))
		}
		c.Logger.Info(fmt.Sprintf("TenancyCacheValue:%s", TenancyCacheValue))
		workCorp, err := repositorie_corp.FindFirstCorp(c)
		c.Logger.Info(fmt.Sprintf("%s", workCorp))
		return nil
	}
}
