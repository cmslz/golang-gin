package queue

import (
	"gin/core/amqp"
	"gin/core/amqp/queue/bin"
	"gin/core/amqp/queue/interface"
	"gin/domain/tenancy/queue"
)

func Dispatches() map[string]queue_interface.QueueInterface {
	return map[string]queue_interface.QueueInterface{
		"demo":                    bin.Demo{},
		amqp.TenancyOpenCorpQueue: queue_teancny.OpenCorp{},
	}
}

func Dispatcher(action string) queue_interface.QueueInterface {
	m, has := Dispatches()[action]
	if !has {
		return nil
	}
	return m
}
