package logger

import (
	"gin/core/configure"
	"github.com/olivere/elastic/v7"
	"go.uber.org/zap"
)

// RegisterLogger // 注册日志
func RegisterLogger(l *zap.SugaredLogger, cf *configure.Configure, es *elastic.Client, TenancyId string) *zap.SugaredLogger {
	log := cf.Log
	l = New(log.Driver, log.Name, log.LowStartLevel, log.HighStartLevel, es, TenancyId).Sugar()
	return l
}
