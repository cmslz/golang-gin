package models

type Log struct {
	Category  string `xml:"category" json:"category" yaml:"category"`
	Level     string `xml:"level" json:"level" yaml:"level"`
	TenantId  string `xml:"tenant_id" json:"tenant_id" yaml:"tenant_id"`
	Content   string `xml:"content" json:"content" yaml:"content"`
	CreatedAt string `xml:"created_at" json:"created_at" yaml:"created_at"`
}
