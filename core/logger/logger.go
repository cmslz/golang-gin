package logger

import (
	"context"
	"encoding/json"
	"fmt"
	"gin/core/logger/models"
	"gin/core/tools"
	"github.com/olivere/elastic/v7"
	"github.com/satori/go.uuid"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"log"
	"os"
)

// ensure we always implement io.WriteCloser
var _ io.WriteCloser = (*Logger)(nil)

type Logger struct {
	Elastic  *elastic.Client
	TenantId string `yaml:"tenant_id" xml:"tenant_id" json:"tenant_id"`
	Name     string `yaml:"name" xml:"name" json:"name"`
}

type Log struct {
	Level  string `json:"level"`
	Ts     string `json:"ts"`
	Caller string `json:"caller"`
}

func New(driver string, name string, lowLevel int, highLevel int, elastic *elastic.Client, tenantId string) *zap.Logger {
	logger := &lumberjack.Logger{
		Filename:   fmt.Sprintf("./logs/%s/%s-%s.log", tools.Now().Format("2006-01"), name, tools.Now().Format("02_15:04")),
		MaxSize:    100, // megabytes
		MaxBackups: 3,
		MaxAge:     7,    //days
		Compress:   true, // disabled by default
		LocalTime:  true,
	}
	return buildLogger(&Logger{
		Elastic:  elastic,
		TenantId: tenantId,
		Name:     name,
	}, logger, zapcore.Level(lowLevel), zapcore.Level(highLevel))
}

func buildLogger(hw io.Writer, lw io.Writer, lowLevel zapcore.Level, highLevel zapcore.Level) *zap.Logger {
	var zapCore zapcore.Core
	hWriter := zapcore.AddSync(hw)
	lWriter := zapcore.AddSync(lw)

	consoleDebugging := zapcore.Lock(os.Stdout)

	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoder := zapcore.NewJSONEncoder(encoderConfig)

	// Join the outputs, encoders, and level-handling functions into
	// zapcore.Cores, then tee the four cores together.
	InfoPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zap.DebugLevel
	})
	highPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= highLevel
	})
	lowPriority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= lowLevel
	})

	zapCore = zapcore.NewTee(
		zapcore.NewCore(encoder, consoleDebugging, InfoPriority),
		zapcore.NewCore(encoder, lWriter, lowPriority),
		zapcore.NewCore(encoder, hWriter, highPriority),
	)

	caller := zap.AddCaller()
	callerSkipOpt := zap.AddCallerSkip(1)

	return zap.New(zapCore, caller, callerSkipOpt, zap.AddStacktrace(zap.ErrorLevel))
}

func (l *Logger) Write(p []byte) (n int, err error) {
	var lo Log
	if err := json.Unmarshal(p, &lo); err != nil {
		return 0, err
	}
	type Info struct {
		Content string
	}
	m := models.Log{
		Category:  lo.Caller,
		Content:   string(p),
		Level:     lo.Level,
		TenantId:  l.TenantId,
		CreatedAt: lo.Ts,
	}
	uuidV4 := uuid.NewV4()
	uuidString := uuidV4.String()
	_, err = l.Elastic.Index().Index(l.Name).Id(uuidString).BodyJson(&m).Do(context.Background())
	if err != nil {
		log.Println(fmt.Sprintf("err:%s", err))
		return 0, err
	}
	return len(p), err
}

// Close implements io.Closer, and closes the current logfile.
func (l *Logger) Close() error {
	return nil
}
