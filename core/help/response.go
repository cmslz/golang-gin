package help

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Data struct {
	Code    uint        `yaml:"code" xml:"code" json:"code"`
	Message string      `yaml:"message" xml:"message" json:"message"`
	Data    interface{} `yaml:"data" xml:"data" json:"data"`
}

// Success //** 成功
func Success(i *gin.Context, data interface{}) {
	i.JSON(http.StatusOK, Data{Code: 0, Message: "ok", Data: data})
}

// Error //** 失败
func Error(i *gin.Context, code uint, message string) {
	i.JSON(http.StatusInternalServerError, Data{Code: code, Message: message, Data: nil})
}
