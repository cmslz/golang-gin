package strutil

import (
	"gopkg.in/gographics/imagick.v3/imagick"
	"strings"
)

const (
	DefaultDPI float64 = 72
)

func AutoWrap(width int, content string, wrap bool, dw *imagick.DrawingWand, mw *imagick.MagickWand) []string {
	result := make([]string, 0)
	if mw == nil || dw == nil || len(content) == 0 {
		return result
	}

	contents := strings.Split(content, "")
	line := ""

	var lineWidth float64 = 0
	for idx, str := range contents {

		if str == "\n" {
			if line != "" {
				result = append(result, line)
				line = ""
				lineWidth = 0
			} else {
				result = append(result, "")
			}
			continue
		}

		if len(str) > 0 {
			info := mw.QueryFontMetrics(dw, str)
			lineWidth += info.TextWidth

			if lineWidth > float64(width) {
				result = append(result, line)
				line = ""
				lineWidth = info.TextWidth
			}
			line += str

			if idx == len(contents)-1 && line != "" {
				result = append(result, line)
			}
		}
	}

	if !wrap && len(result) > 0 {
		str := result[0]
		strs := strings.Split(str, "")
		wrapStr := str
		if len(strs) < len(contents) && len(strs) > 0 {
			wrapStr = strings.Join(append(strs[:len(strs)-1], "…"), "")
		}
		result = []string{wrapStr}
	}

	return result
}
