package httputil

import (
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// 下载数据
func Download(link string, timeout time.Duration) ([]byte, error) {
	if !strings.HasPrefix(link, "http") {
		return nil, errors.New(link + "不是有效的链接")
	}
	req, err := http.NewRequest("GET", link, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{
		Timeout: time.Second * timeout,
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}
