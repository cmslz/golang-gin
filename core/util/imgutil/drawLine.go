package imgutil

import (
	"gopkg.in/gographics/imagick.v3/imagick"
	"math"
)

func DrawLine(canvas *imagick.MagickWand, width float64, height float64, x int, y int, color string) error {
	mw := imagick.NewMagickWand()
	defer mw.Destroy()
	pw := imagick.NewPixelWand()
	defer pw.Destroy()

	pw.SetColor(color)

	err := mw.NewImage(uint(width), uint(height/10), pw)
	if err != nil {
		return err
	}
	return canvas.CompositeImage(mw, imagick.COMPOSITE_OP_OVER, true, int(x), int(float64(y)+math.Ceil(height/2)))
}
