package imgutil

// 图片解码
import (
	"bytes"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
)

func Decode(data []byte) (image.Image, error) {
	img, err := png.Decode(bytes.NewReader(data))
	if err != nil {
		img, err = jpeg.Decode(bytes.NewReader(data))
	}
	if err != nil {
		img, err = gif.Decode(bytes.NewReader(data))
	}
	if err != nil {
		return nil, errors.New("图片解析失败")
	}
	return img, nil
}
