package core_gorm

import (
	"github.com/jinzhu/gorm"
)

// updateTimeStampForCreateCallback will set `CreatedAt`, `UpdatedAt` when creating
func UpdateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		timestamp := gorm.NowFunc().Format("2006-01-02 15:04:05")

		if createdAtField, ok := scope.FieldByName("CreatedAt"); ok {
			if createdAtField.IsBlank {
				_ = createdAtField.Set(timestamp)
			}
		}

		if updatedAtField, ok := scope.FieldByName("UpdatedAt"); ok {
			if updatedAtField.IsBlank {
				_ = updatedAtField.Set(timestamp)
			}
		}
	}
}
