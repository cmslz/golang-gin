package core_gorm

import "github.com/jinzhu/gorm"

type Db struct {
	gorm.DB
}
