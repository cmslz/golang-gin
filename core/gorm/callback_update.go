package core_gorm

import "github.com/jinzhu/gorm"

// updateTimeStampForUpdateCallback will set `UpdatedAt` when updating
func UpdateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		timestamp := gorm.NowFunc().Format("2006-01-02 15:04:05")
		_ = scope.SetColumn("UpdatedAt", timestamp)
	}
}
