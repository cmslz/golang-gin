package databases

import (
	"gin/core/gorm"
	"github.com/jinzhu/gorm"
	"go.uber.org/zap"
)

// RegisterDb /** 注册db
func RegisterDb(d *gorm.DB, Debug bool, IdlePool int, Pool int, l *zap.SugaredLogger, buildMysqlConn string) *gorm.DB {
	db, err := gorm.Open("mysql", buildMysqlConn)
	if err != nil {
		l.Fatalf("mysql connect failed %s", err)
	}

	// 表名字设置
	db.SingularTable(true)
	// 是否开启日志
	db.LogMode(Debug)
	// 设置连接池
	db.DB().SetMaxIdleConns(IdlePool)
	db.DB().SetMaxOpenConns(Pool)

	// 注册插件 将默认 orm deleted_at 由 date 切换成 timestamp
	db.Callback().Create().Replace("gorm:update_time_stamp", core_gorm.UpdateTimeStampForCreateCallback)
	db.Callback().Update().Replace("gorm:update_time_stamp", core_gorm.UpdateTimeStampForUpdateCallback)

	err = db.DB().Ping()
	if err != nil {
		l.Fatalf("mysql connect failed %s", err)
	}
	db = db.Unscoped()
	if Debug {
		d = db.Debug()
	} else {
		d = db
	}
	return d
}
