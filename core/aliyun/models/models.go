package model_aliyun

type Aliyun struct {
	RegionId        string `yaml:"region_id"`
	AccessKeyId     string `yaml:"access_key_id"`
	AccessKeySecret string `yaml:"access_key_secret"`
	DomainName      string `yaml:"domain_name"`
	DomainIp        string `yaml:"domain_ip"`
	DomainType      string `yaml:"domain_type"`
	DomainTtl       int64  `yaml:"domain_ttl"`
}
