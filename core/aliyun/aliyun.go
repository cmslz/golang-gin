package aliyun

import (
	"gin/core/aliyun/models"
	client2 "github.com/alibabacloud-go/alidns-20150109/client"
	"github.com/alibabacloud-go/darabonba-openapi/client"
)

type AliyunClient struct {
	Config       *client.Config
	Client       *client2.Client
	AliyunConfig *model_aliyun.Aliyun
}

func NewClient(aliyun *model_aliyun.Aliyun) (*AliyunClient, error) {
	aliyunClient := &AliyunClient{
		Config: &client.Config{
			AccessKeyId:     &aliyun.AccessKeyId,
			AccessKeySecret: &aliyun.AccessKeySecret,
			RegionId:        &aliyun.RegionId,
		},
		AliyunConfig: aliyun,
	}
	newClient, err := client2.NewClient(aliyunClient.Config)
	if err != nil {
		return nil, err
	}
	aliyunClient.Client = newClient
	return aliyunClient, nil
}

// DomainAllList // 获取所有解析列表
func (ac *AliyunClient) DomainAllList(domain *string, RR *string, recordType *string) (*client2.DescribeDomainRecordsResponse, error) {
	req := client2.DescribeDomainRecordsRequest{}
	req.DomainName = domain
	req.RRKeyWord = RR
	req.Type = recordType
	return ac.Client.DescribeDomainRecords(&req)
}

// CreateDomainRecord // 创建分析记录
func (ac *AliyunClient) CreateDomainRecord(Rr *string) (*client2.AddDomainRecordResponse, error) {
	req := client2.AddDomainRecordRequest{}
	req.DomainName = &ac.AliyunConfig.DomainName
	req.RR = Rr
	req.Type = &ac.AliyunConfig.DomainType
	req.TTL = &ac.AliyunConfig.DomainTtl
	req.Value = &ac.AliyunConfig.DomainIp
	return ac.Client.AddDomainRecord(&req)
}
