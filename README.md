# GIN https://gitee.com/cmslz/golang-gin
## 代码结构
```
├── assets 静态文件  
│   └── font
│       ├── Alibaba-PuHuiTi-Bold.otf
│       ├── Alibaba-PuHuiTi-Heavy.otf
│       ├── Alibaba-PuHuiTi-Light.otf
│       ├── Alibaba-PuHuiTi-Medium.otf
│       ├── Alibaba-PuHuiTi-Regular.otf
│       ├── Microsoft Yahei.ttf
│       ├── PingFang Regular.ttf
│       ├── SourceHanSansCN-Bold.otf
│       ├── SourceHanSansCN-ExtraLight.otf
│       ├── SourceHanSansCN-Heavy.otf
│       ├── SourceHanSansCN-Light.otf
│       ├── SourceHanSansCN-Medium.otf
│       ├── SourceHanSansCN-Normal.otf
│       └── SourceHanSansCN-Regular.otf
├── bin  执行文件
│   └── artisan.go
├── config  配置
│   ├── env-example.yml
│   └── env.yml
├── core  核心库
│   ├── aliyun 阿里云扩展 
│   │   ├── aliyun.go
│   │   └── models
│   │       └── models.go
│   ├── amqp  Amqp扩展
│   │   ├── amqp.go
│   │   ├── queue
│   │   │   ├── bin
│   │   │   │   └── demo.go
│   │   │   ├── interface
│   │   │   │   └── interface.go  全局队列interface继承类
│   │   │   └── queue.go  全局队列注册
│   │   ├── queue.go
│   │   └── services.go
│   ├── cache  缓存扩展
│   │   ├── cache.go
│   │   └── services.go
│   ├── configure  配置项扩展
│   │   ├── configure.go
│   │   └── models.go
│   ├── core.go  核心处理类
│   ├── databases 数据库扩展
│   │   └── services.go
│   ├── gorm  数据库操作扩展
│   │   ├── callback_create.go
│   │   ├── callback_update.go
│   │   ├── db.go
│   │   └── utils.go
│   ├── help  帮助扩展类
│   │   ├── help.go
│   │   └── response.go
│   ├── logger 日志扩展类
│   │   ├── logger.go
│   │   ├── models
│   │   │   └── modles.go
│   │   └── services.go
│   ├── pool  多线程扩展类
│   │   ├── README.md
│   │   └── pool.go
│   ├── tools  工具类
│   │   └── tools.go
│   └── util   工具扩展类
│       ├── httputil
│       │   └── download.go
│       ├── imgutil
│       │   ├── decoder.go
│       │   └── drawLine.go
│       └── strutil
│           └── wrap.go
├── domain  领域入口
│   ├── corp 领域
│   │   ├── models 领域模型
│   │   │   └── models.go
│   │   ├── ports  领域端口 
│   │   │   └── merchant 后台
│   │   │       ├── controllers  控制层
│   │   │       │   └── CorpController.go
│   │   │       └── router.go  路由注册
│   │   ├── repositories  仓库层
│   │   │   └── CorpRepository.go
│   │   ├── router.go  路由注册
│   │   └── services  服务层
│   ├── poster  海报域
│   │   ├── ports
│   │   │   └── merchant_poster
│   │   │       ├── controllers
│   │   │       │   └── PosterController.go 海报合成demo
│   │   │       └── router.go
│   │   ├── router.go
│   │   └── services
│   │       ├── README.md
│   │       ├── cache.go
│   │       ├── composer.go
│   │       ├── composite.go
│   │       ├── imagickPainter.go
│   │       ├── mask.go
│   │       └── masks.json
│   └── tenancy  租户领域
│       ├── middleware 租户中间件
│       │   └── middleware.go  租户主要逻辑中间件
│       ├── models
│       │   ├── domain.go 租户域名模型
│       │   ├── models.go 租户模型
│       │   └── tenantCorp.go 租户企业模型
│       ├── ports
│       │   └── platform  平台端 
│       │       ├── controllers
│       │       │   └── TenantCorpController.go 开户
│       │       └── router.go
│       ├── queue
│       │   └── OpenCorpQueue.go  开户队列
│       ├── repositories
│       │   ├── domainRepository.go 
│       │   ├── tenancyRepository.go
│       │   └── tenantCorpRepository.go
│       ├── router.go
│       └── services
│           ├── TenantCorpService.go
│           └── services.go
├── migrations  迁移脚本
│   ├── databases  数据库迁移
│   │   └── migrations_databases_demo.go
│   ├── interface
│   │   └── interface.go  迁移脚本继承类
│   ├── migrations.go  迁移脚本注册入口
│   └── script  脚本迁移
│       └── migretions_script_demo.go
├── router
│   └── router.go  路由主入口
├── IMAGICK.md  海报合成安装文档
├── README.md 项目主要说明
├── go.mod
├── go.sum
├── main.go 执行入口
```
## 环境安装
### 海报服务 http://www.voidcc.com/project/imagick
## 脚本迁移
### 迁移文件
```
go run main.go migrate/up [action]
go run ./bin/artisan.go migrate/down [action]
```
#### Demo
```
go run ./bin/artisan.go migrate/up test // echo "hello word"
```
## Amqp
### 入队列
```
var _content json.RawMessage
content, err = json.Marshal([]byte(`{"id":1}`))
Demo := amqp.PublishConfig{
    QueueName: "demo",
    Content: amqp.Content{
        TenancyId: c.TenancyId,
        Content:   content,
    },
}
err = amqp.PublishSimple(c.Logger, c.RabbitMQ, Demo)
if err != nil {
    c.Logger.Fatalf(fmt.Sprintf("%s", err))
}
```
### 队列消费
```
go run ./bin/artisan.go work:queue [队列名称] args...[额外参数]
```

## 部署
### 初始化项目
```
go mod tidy
```

### 热更新-直接执行
```
fresh
```

### 正式环境
```
go run ./main.go
```


## 环境变量配置([env.yml](config/env.yml))

## 数据表说明
### 租户说明
- 路由租户中间件 `c.Engine.Use(middleware_tenancy.MiddleWare(c))` 使用中间件才可正常使用租户领域

### 中央域表
#### 租户表
```sql
CREATE TABLE `tenants` (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `data` json DEFAULT NULL COMMENT '租户数据库信息',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='租户表';
```
#### 租户企业表
```sql
CREATE TABLE `tenant_corp` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '企业id',
  `tenant_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
  `logo` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '企业logo',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '企业名称',
  `short_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '企业简称',
  `qr_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '企业二维码',
  `contact` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '联系人',
  `contact_telephone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '联系电话',
  `district` smallint DEFAULT NULL COMMENT '地区编码',
  `longitude` decimal(10,6) DEFAULT NULL COMMENT '经度',
  `latitude` decimal(10,6) DEFAULT NULL COMMENT '纬度',
  `address` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
  `config` json DEFAULT NULL COMMENT '企业信息',
  `status` smallint NOT NULL DEFAULT '0' COMMENT '企业状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='租户-企业';
```
#### 租户域名表
```sql
CREATE TABLE `tenant_domains` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `domain` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户域名',
  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '关联租户id',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `domains_domain_unique` (`domain`) USING BTREE,
  KEY `domains_tenant_id_foreign` (`tenant_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='租户域名表';
```
### `tenant_base`  租户主库 所有新开户的数据库都会同步改库内容
